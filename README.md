# Image Optimisation Readme #

This is a small image optimisation package that will

* Optimise jpg, gif, png and svg's assets
* Create optimised WebP formats for jpg, gif and png's
* Maintains the original folder structure

## How do I get set up? ##

* Clone the repository 
* CLI into the folder and run `npm i` to install the required npm packages
* Copy any images you have into the "original-images" folder i.e optimise-images > original-images, this folder can also contain subfolders
* Run the command `npm run images` to optimise all the images - the new optimised images will be created into a folder called optimised-images along with a webp folder and copies
* You can clear the image cache by running the command `npm run clear cache`
* You can also run individual commands, for example, to just run the webp function use `gulp webp`

## Image Compression Settings ##

We are using the [gulp-imagemin](https://www.npmjs.com/package/gulp-imagemin) package to optimise the images and all compression settings are located within the imagemin function in gulpfile.js

I have experimented with many settings and find the current settings to work the best overall. That being said please review the optimised images and check they are not over-optimised for your requirements. 