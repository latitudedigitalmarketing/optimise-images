var gulp = require('gulp');
var cache = require('gulp-cache');
var imagemin = require('gulp-imagemin');
var imageminPngquant = require('imagemin-pngquant');
var imageminZopfli = require('imagemin-zopfli');
var imageminMozjpeg = require('imagemin-mozjpeg');
var imageminWebp = require('imagemin-webp');
var runSequence = require('run-sequence');
var imageminGifsicle = require('imagemin-gifsicle');
var rename = require("gulp-rename");
var extReplace = require("gulp-ext-replace");
var imageminGiflossy = require('imagemin-giflossy');

// compress all images
gulp.task('imagemin', function () {
    return gulp.src(['original-images/**/*.{png,jpg,svg,gif}'])
        .pipe(cache(imagemin([
            //png
            imageminPngquant({
                speed: 1, // The lowest speed of optimization with the highest quality
                quality: '70-90', // When used more then 70 the image wasn't saved
                floyd: 1 // Controls level of dithering (0 = none, 1 = full).
            }),
            imageminZopfli({
                more: true
                // iterations: 50 // very slow but more effective
            }),
            //gif
            imageminGifsicle({
                optimizationLevel: 3,
                colors: 64,
                lossy: 80,
            }),
            //svg
            imagemin.svgo({
                plugins: [{
                    removeViewBox: false
                }]
            }),
            //jpg lossless
            imagemin.jpegtran({
                progressive: true
            }),
            //jpg very light lossy, use vs jpegtran
            imageminMozjpeg({
                quality: 70
            })
        ])))
        .pipe(gulp.dest('optimised-images')),
        console.log('images optimized');
});

gulp.task("webp", function () {
    let src = 'original-images/**/*.{png,jpg,gif}';
    let dest = "optimised-images/webp";

    return gulp.src(src)
        .pipe(imagemin([
            imageminWebp({
                quality: 50
            })
        ]))
        .pipe(extReplace(".webp"))
        .pipe(gulp.dest(dest)),
        console.log('webp images created');
});

// clear the image cache 
gulp.task('clear', () =>
    cache.clearAll()
);

// lets run a sequence of events
gulp.task('images', function (callback) {
    runSequence('imagemin', function() {
        callback();
    });
    runSequence('webp');
});